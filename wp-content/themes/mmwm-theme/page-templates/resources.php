<?php
/**
 * Template Name: resources
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;

?>
<!-- content start -->

<div class="page-container">
	<div class="content-container">
		<div class="row">
			<div class="col-md-12">
				<h1>Resources</h1>
				<h2>Learn more about psychedelic research.</h2>
				<p>We aim to share research renaissance to the general public, there is a large global community out there. Explore the links on this page to learn more about what's happening with psychedelics worldwide.</p>

				<h3>Videos from Psychedelic Science 2017</h3>
				<p><a href="#" target="_blank" class="linkOut">Visit website</a></p>
				<p>At Psychedelic Science 2017, scientists, doctors, therapists, students, educators, policymakers, artists, and others from around the world come together to share and discover recently completed and ongoing research into the risks and benefits of psychedelics and medical marijuana for science, medicine, spirituality, creativity, and more.</p>

				<h3>Maps Resources</h3>
				<p><a href="#" target="_blank" class="linkOut">Visit website</a></p>
				<p>This year, millions of people will use psychedelics outside of supervised medical contexts, many of them for the first time. The Zendo Project provides a supportive environment and specialized care to transform difficult psychedelic experiences into valuable learning opportunities, and potentially offer healing and growth.</p>

				<h3>Beckley Foundation</h3>
				<p><a href="#" target="_blank" class="linkOut">Visit website</a></p>
				<p>The Beckley Foundation is a UK-based think-tank and NGO founded and directed by Amanda Feilding. Its purpose is to pioneer psychedelic research and drive evidence-based drug policy reform.</p>

				<h3>The Zendo Project</h3>
				<p><a href="#" target="_blank" class="linkOut">Visit website</a></p>
				<p>This year, millions of people will use psychedelics outside of supervised medical contexts, many of them for the first time. The Zendo Project provides a supportive environment and specialized care to transform difficult psychedelic experiences into valuable learning opportunities, and potentially offer healing and growth.</p>



			</div>
		</div>
	</div>
</div>

<!-- content end -->
<?php

get_footer();
