<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono" rel="stylesheet">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-oi8o31xSQq8S0RpBcb4FaLB8LJi9AT8oIdmS1QldR8Ui7KUQjNAnDlJjp55Ba8FG" crossorigin="anonymous">


</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">


	<!-- TODO https://tympanus.net/codrops/2018/04/10/webgl-distortion-hover-effects/ -->
	<div class="mmwm-wrapper">

		<!-- not a container-fluid -->
		<div class="container">

			<div class="row menu-row">

				<!-- TODO http://www.cyan-music.com/releases-084.html -->

				<div class="col-xl-3 col-lg-4 col-md-4">
					<!-- <div class="music-player">
						<div class="player">
							<div class="icon-text">
								<i class="fas fa-play"></i>
								<span class="text">Click to play music</span>
							</div>
							<div class="now-playing displayNone">
								TODO jQuery appear disappear
								<span>Now Playing:<br><a href="http://www.cyan-music.com/releases-084.html" target="_blank">D-Echo Project - Fresh Breeze</a></span>
							</div>
						</div>
					</div> -->
					<div class="main-menu">
						<div class="logo">
							<a href="/">
								<img src="/wp-content/uploads/2018/06/mapping-the-mind-with-mushrooms-blank-bg.png" alt="">
							</a>
						</div>
						<div class="copy">
							<div class="intro">
								<p>September 22, 2018</p>
								<p>5 Bancroft Ave<br>Earth Sciences Center<br>University of Toronto<br>Toronto, Canada</p>
							</div>
							<div class="cta">
								<!-- TODO https://tympanus.net/Development/ParticleEffectsButtons/ -->
								<a href="/event/mapping-the-mind-with-mushrooms/" class="a-cta">Buy Tickets <i class="far fa-ticket-alt"></i></a>
							</div>
							<div class="links">
								<a href="/the-cause/">The Cause</a>
								<a href="/resources/">Resources</a>
								<!-- <a href="#">2017</a> -->
								<!-- <a href="#">2016</a> -->
								<!-- <a href="#">Contact</a> -->
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-7 col-lg-8 col-md-8" id="bodyContent">
					<!-- EVERYTHING HERE IS WHAT IS ON THE TEMPLATE PAGE -->
