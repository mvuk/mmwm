<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mushroom_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A{F[0-SpI!B0d?.{}aZxBri^,y&5to0-g|Rl6bUMZt_1t3w8b]x2wG1NZe1#;2pR');
define('SECURE_AUTH_KEY',  'pDQvMpWzR<HV~S+(Qs$X2^O)FAJE{E5RAnv NDdvjgC_Bf>e/G?}%?dUnS`yQ-9j');
define('LOGGED_IN_KEY',    '5Q!YiXc#9M~-awO!(8MA!p@0}$x{N H51~{$CjL;&`ChxW5 !4>d!bkXo6n;Y*)@');
define('NONCE_KEY',        'up#4t!]`2Kwal1wWyHZr(qjcsAHP7YDST?LI6F:xX*`59Qi=BJj<:{G&K~y,`ov%');
define('AUTH_SALT',        'xBHtv T[J<wgl1/gksr;7?sh~RV/WA-B-a@pVsoqx2T_ZNU-D<[S(Fb1SxA$Hp@%');
define('SECURE_AUTH_SALT', '&J24J2;Mwe9otY[o8Iwx#mss(k,r+Tt%[9p& EXZYY^ootgd7^W)%v<> cGcdN~S');
define('LOGGED_IN_SALT',   ':Zan#3N/(xDOHG_L282LV}X!1T<}9v3D1YmG;WJ4BEUU@iG#S$TE&;OSO.ZF[efB');
define('NONCE_SALT',       '-2ay2j+[K!2Jp(NQI.^>8ezkGx?P29ofV||@~z9Fvjy$Eg^/=qfWL3__e@A6UuDJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
